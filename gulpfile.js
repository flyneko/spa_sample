var gulp = require('gulp');
var coffee = require('gulp-coffee');
var concat = require('gulp-concat');
var uglify  = require('gulp-uglify');

gulp.task('coffee', function () {
    gulp.src([
        'app/models/TextBlockModel.coffee',
        'app/models/MegaTextBlockModel.coffee',
        'app/views/TextBlockView.coffee',
        'app/views/MegaTextBlockView.coffee',
        'app/views/TextBlockListView.coffee',
        'app/collections/TextBlockCollection.coffee',
        'app/general.coffee',
    ])
        .pipe(concat('a.coffee'))
        .pipe(coffee())
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(gulp.dest(''))
})

gulp.task('default', ['coffee']);