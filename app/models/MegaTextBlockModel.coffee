class MegaTextBlock extends TextBlock
  defaults: () ->
    defaults =
      color: 'green'

    _.extend(defaults, TextBlock.prototype.defaults)

  initialize: () ->
    TextBlock.prototype.initialize.apply @
    @view = new MegaTextBlockView({ model: @ });
    @bind('change:color', (block, color) ->
      block.view.$el.css('background', block.view.colors[color])
    )
