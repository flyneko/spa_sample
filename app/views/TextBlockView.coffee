class TextBlockView extends Backbone.View
  events:
    'click'                         : 'clickHandler'
    'click .delete-block'           : 'removeBlock'
    'click .block-text span'        : 'editText'
    'mouseout'                      : 'textFromAreaToModel'
    'click .block-text textarea'    : (e) -> e.stopPropagation()
  template: _.template('<div class="block" data-block-id="<%= id %>"><button type="button" class="close delete-block" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <div class="block-text"><span><%= text %></span></div></div>'),

  render: () ->
    variables     = @model.attributes
    variables.id  = @model.cid
    @$el          = $(@template(variables))
    @delegateEvents()

    if @model.get('selected')
      @$el.addClass('selected')

    @$el

  removeBlock: () ->
    blockList.collection.remove(@model)

  clickHandler: () ->
    @selectBlock()

  editText: (e) ->
    e.stopPropagation()
    $textarea = $('<textarea>', {class: 'form-control'});
    $textarea.css('height', @$el.find('.block-text span').height() + 20).val(@model.get('text'))
    @$el.find('.block-text').empty().append($textarea)

  textFromAreaToModel: () ->
    if !@$el.find('textarea').length
      return
    text = @$el.find('textarea').val()
    @model.set('text', text)
    @$el.find('.block-text').html("<span>#{text}</span>")
    @delegateEvents()

  selectBlock: () ->
    @$el.toggleClass('selected')
    @model.set('selected', !@model.get('selected'))