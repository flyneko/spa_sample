class MegaTextBlockView extends TextBlockView
  clicks: 0
  timer:  null
  colors:
    green:  '#D8FFD3'
    red:    '#FFD2D1'

  events:
    'dblclick': (e) -> e.preventDefault()

  initialize: () ->
    _.extend(@events, TextBlockView.prototype.events)

  render: () ->
    @$el = TextBlockView.prototype.render.apply @
    @$el.css('background', @colors[@model.get('color')])

  clickHandler: () ->
    self = @
    @clicks++

    if @clicks is 1
      @timer = setTimeout(( ->
        self.selectBlock()
        self.clicks = 0
      ), 180)
    else
      clearTimeout(@timer)
      @clicks = 0

      if @model.get('color') is 'green'
        @model.set('color', 'red')
      else
        @model.set('color', 'green')

  removeBlock: (e) ->
    e.stopPropagation()

    if !confirm('Вы уверены?')
      return

    TextBlockView.prototype.removeBlock.apply @