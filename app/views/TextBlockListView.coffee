class TextBlockList extends Backbone.View
  el: '#blocks-wrapper'
  events:
    'click .add-block-button':      () -> @collection.add(new TextBlock())
    'click .add-mega-block-button': () -> @collection.add(new MegaTextBlock())

  initialize: () ->
    self        = @
    @collection = new TextBlockCollection()

    @listenTo(@collection, 'add', (block) ->
      self.$el.find('.blocks-container').prepend(block.view.render())
    )

    @listenTo(@collection, 'change add remove reset', () ->
      localStorage.blocks = JSON.stringify(@collection)
      counters            = { general: 0, selected: 0, red: 0, green: 0 }

      # Update counters
      @collection.each((block) ->
        counters.general++
        if block.get('selected')
          counters.selected++
        if block.get('color')
          counters[block.get('color')]++
      )

      for i of counters
        @$el.find("[data-counter=#{i}]").html(counters[i])
    )

    @listenTo(@collection, 'remove', (block) ->
      block.view.remove()
    )

    if localStorage.blocks
      blocksStorage = JSON.parse(localStorage.blocks)
      for block in blocksStorage
        if block.color
          @collection.add(new MegaTextBlock(block))
        else
          @collection.add(block)